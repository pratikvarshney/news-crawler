/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.usc.csci572.pvarshne.hw2;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class CrawlStat {
	private long fetchesAttempted;
	private long fetchesSucceeded;
	private long fetchesFailed;

	private List<VisitStat> visitedUrlStatList;
	private List<FetchStat> fetchedUrlStatList;
	private Set<String> extractedUrlSet;

	private long numberOfExtractedUrls;

	public CrawlStat() {
		visitedUrlStatList = new LinkedList<VisitStat>();
		fetchedUrlStatList = new LinkedList<FetchStat>();
		extractedUrlSet = new HashSet<String>();
	}

	// fetchesAborted = fetchesAttempted - (fetchesSucceeded + fetchesFailed);
	public long getFetchesAborted() {
		return fetchesAttempted - (fetchesSucceeded + fetchesFailed);
	}

	public long getFetchesAttempted() {
		return fetchesAttempted;
	}

	public void incFetchesAttempted() {
		this.fetchesAttempted++;
	}

	public long getFetchesSucceeded() {
		return fetchesSucceeded;
	}

	public void incFetchesSucceeded() {
		this.fetchesSucceeded++;
	}

	public long getFetchesFailed() {
		return fetchesFailed;
	}

	public void incFetchesFailed() {
		this.fetchesFailed++;
	}

	public List<VisitStat> getVisitedUrlStatList() {
		return visitedUrlStatList;
	}

	public List<FetchStat> getFetchedUrlStatList() {
		return fetchedUrlStatList;
	}

	public Set<String> getExtractedUrlSet() {
		return extractedUrlSet;
	}

	public long getNumberOfExtractedUrls() {
		return numberOfExtractedUrls;
	}

	public void incNumberOfExtractedUrls(long numberOfExtractedUrls) {
		this.numberOfExtractedUrls += numberOfExtractedUrls;
	}
}