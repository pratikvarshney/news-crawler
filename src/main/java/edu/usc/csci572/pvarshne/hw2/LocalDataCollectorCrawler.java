/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.usc.csci572.pvarshne.hw2;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;

public class LocalDataCollectorCrawler extends WebCrawler {
	private static final Logger logger = LoggerFactory.getLogger(LocalDataCollectorCrawler.class);

	private final static Pattern FILTERS = Pattern.compile(
			".*(\\.(css|js|mid|mp2|mp3|mp4|wav|avi|mov|mpeg|ram|m4v|rm|smil|wmv|swf|wma|zip|rar|gz|rss|xml|json|map))$");

	CrawlStat myCrawlStat;

	public LocalDataCollectorCrawler() {
		myCrawlStat = new CrawlStat();
	}

	/**
	 * This function is called once the header of a page is fetched. It can be
	 * overridden by sub-classes to perform custom logic for different status
	 * codes. For example, 404 pages can be logged, etc.
	 *
	 * @param webUrl
	 *            WebUrl containing the statusCode
	 * @param statusCode
	 *            Html Status Code number
	 * @param statusDescription
	 *            Html Status COde description
	 */
	@Override
	protected void handlePageStatusCode(WebURL webUrl, int statusCode, String statusDescription) {
		logger.info("Attempted URL: {}, StatusCode: {}, {}", webUrl.getURL(), statusCode, statusDescription);
		myCrawlStat.incFetchesAttempted();
		myCrawlStat.getFetchedUrlStatList().add(new FetchStat(webUrl.getURL(), statusCode));
		if (statusCode >= 200 && statusCode <= 299) {
			// succeeded
			logger.info("Succeeded URL: {}", webUrl.getURL());
			myCrawlStat.incFetchesSucceeded();
		} else {
			// failed or aborted
			if (statusCode >= 300 && statusCode <= 399) {
				// aborted
				logger.info("Aborted URL: {}, StatusCode: {}, {}", webUrl.getURL(), statusCode, statusDescription);
				// value is calculated later on
			} else {
				// failed
				logger.info("Failed URL: {}, StatusCode: {}, {}", webUrl.getURL(), statusCode, statusDescription);
				myCrawlStat.incFetchesFailed();
			}
		}
	}

	@Override
	public boolean shouldVisit(Page referringPage, WebURL url) {
		String href = url.getURL().toLowerCase();
		if (href.startsWith("http://www.nytimes.com/") || href.startsWith("https://www.nytimes.com/")
				|| href.startsWith("http://nytimes.com/") || href.startsWith("https://nytimes.com/")) {
			try {
				URL u = new URL(url.getURL());
				if (!(FILTERS.matcher(u.getPath()).matches() || href.contains("nytimes.com/timeswire/feeds"))) {
					return true;
				}
			} catch (MalformedURLException e) {
				logger.error("Error", e);
			}
		}
		return false;
	}

	@Override
	public void visit(Page page) {
		// calc visit stats
		long size = page.getContentData().length;
		long numberOfOutlinks = 0;
		String contentType = page.getContentType();
		if (contentType != null) {
			contentType = contentType.split(";")[0].trim().toLowerCase();
		}

		if (contentTypeAllowed(contentType)) {
			if (page.getParseData() instanceof HtmlParseData) {
				HtmlParseData parseData = (HtmlParseData) page.getParseData();
				Set<WebURL> links = parseData.getOutgoingUrls();
				numberOfOutlinks = links.size();
				myCrawlStat.incNumberOfExtractedUrls(numberOfOutlinks);
				for (WebURL webUrl : links) {
					myCrawlStat.getExtractedUrlSet().add(webUrl.getURL());
				}
			}

			// save visit stats
			logger.info("Visited URL: {}, contentType: {}, size: {}, outlinks: {}, Content-Type: {}",
					page.getWebURL().getURL(), contentType, size, numberOfOutlinks, page.getContentType());
			myCrawlStat.getVisitedUrlStatList()
					.add(new VisitStat(page.getWebURL().getURL(), size, numberOfOutlinks, contentType));
		} else {
			logger.info("Content-Type Not Allowed: {}, contentType: {}, Content-Type: {}", page.getWebURL().getURL(),
					contentType, page.getContentType());
		}
	}

	private boolean contentTypeAllowed(String contentType) {
		if (contentType == null || "".equals(contentType.trim())) {
			return false;
		}
		// HTML
		if (contentType.contains("text") && contentType.contains("html")) {
			return true;
		}
		// doc or docx
		if (contentType.contains("application/msword")
				|| contentType.contains("application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
			return true;
		}
		// pdf
		if (contentType.contains("application") && contentType.contains("pdf")) {
			return true;
		}
		// image
		if (contentType.contains("image")) {
			return true;
		}
		return false;
	}

	/**
	 * This function is called by controller to get the local data of this
	 * crawler when job is finished
	 */
	@Override
	public Object getMyLocalData() {
		return myCrawlStat;
	}

	/**
	 * This function is called by controller before finishing the job. You can
	 * put whatever stuff you need here.
	 */
	@Override
	public void onBeforeExit() {
		dumpMyData();
	}

	public void dumpMyData() {
		int id = getMyId();
		// You can configure the log to output to file
		logger.info("Crawler {} > Fetches Attempted: {}", id, myCrawlStat.getFetchesAttempted());
		logger.info("Crawler {} > Fetches Succeeded: {}", id, myCrawlStat.getFetchesSucceeded());
		logger.info("Crawler {} > Fetches Aborted: {}", id, myCrawlStat.getFetchesAborted());
		logger.info("Crawler {} > Fetches Failed: {}", id, myCrawlStat.getFetchesFailed());
	}
}