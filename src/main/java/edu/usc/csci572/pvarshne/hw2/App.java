/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.usc.csci572.pvarshne.hw2;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;

public class App {
	private static final Logger logger = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) throws Exception {
		long start = System.currentTimeMillis();
		String crawlStorageFolder = "/media/pratik/Data/USC/CSCI 572/hw2/hw2/data/crawl/root";
		int numberOfCrawlers = 5;
		int maxPagesToFetch = 20000;
		int maxDepthOfCrawling = 16;

		CrawlConfig config = new CrawlConfig();
		config.setCrawlStorageFolder(crawlStorageFolder);
		config.setIncludeBinaryContentInCrawling(true);
		config.setMaxPagesToFetch(maxPagesToFetch);
		config.setMaxDepthOfCrawling(maxDepthOfCrawling);
		config.setConnectionTimeout(150000);
		config.setSocketTimeout(150000);

		logger.info("Crawler config:");
		logger.info(config.toString());

		PageFetcher pageFetcher = new PageFetcher(config);
		RobotstxtConfig robotstxtConfig = new RobotstxtConfig();
		RobotstxtServer robotstxtServer = new RobotstxtServer(robotstxtConfig, pageFetcher);
		CrawlController controller = new CrawlController(config, pageFetcher, robotstxtServer);

		controller.addSeed("http://www.nytimes.com/");
		controller.start(LocalDataCollectorCrawler.class, numberOfCrawlers);

		List<Object> crawlersLocalData = controller.getCrawlersLocalData();

		StatWriter.writeFetchFile(crawlersLocalData, "fetch_NYTimes.csv");
		StatWriter.writeVisitFile(crawlersLocalData, "visit_NYTimes.csv");
		StatWriter.writeStatsFile(crawlersLocalData, "CrawlReport_NYTimes.txt", String.format(
				"Name: %s\nUSC ID: %s\nNews site crawled: %s\n\n", "Pratik Varshney", "7202041752", "nytimes.com"));
		logger.info("Total time taken: {} ms", System.currentTimeMillis() - start);
	}
}