package edu.usc.csci572.pvarshne.hw2;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.apache.http.impl.EnglishReasonPhraseCatalog;

public class StatWriter {

	public static void writeStatsFile(List<Object> crawlersLocalData, String reportTxtFileName, String reportHeader)
			throws IOException {

		PrintWriter reportTxtFile = new PrintWriter(new FileWriter(reportTxtFileName));

		// write header
		reportTxtFile.print(reportHeader);

		reportTxtFile.println("Fetch Statistics");
		reportTxtFile.println("================");
		long totalFetchesAttempted = 0;
		long totalFetchesSucceeded = 0;
		long totalFetchesAborted = 0;
		long totalFetchesFailed = 0;
		for (Object localData : crawlersLocalData) {
			CrawlStat stat = (CrawlStat) localData;
			totalFetchesAttempted += stat.getFetchesAttempted();
			totalFetchesSucceeded += stat.getFetchesSucceeded();
			totalFetchesAborted += stat.getFetchesAborted();
			totalFetchesFailed += stat.getFetchesFailed();
		}
		reportTxtFile.println("# fetches attempted: " + totalFetchesAttempted);
		reportTxtFile.println("# fetches succeeded: " + totalFetchesSucceeded);
		reportTxtFile.println("# fetches aborted: " + totalFetchesAborted);
		reportTxtFile.println("# fetches failed: " + totalFetchesFailed);
		reportTxtFile.println();

		reportTxtFile.println("Outgoing URLs:");
		reportTxtFile.println("==============");
		long totalNumberOfExtractedUrls = 0;
		long uniqueURLsWithinNewsSite = 0;
		Set<String> extractedUrlSet = new HashSet<String>();
		for (Object localData : crawlersLocalData) {
			CrawlStat stat = (CrawlStat) localData;
			totalNumberOfExtractedUrls += stat.getNumberOfExtractedUrls();
			extractedUrlSet.addAll(stat.getExtractedUrlSet());
		}
		for (String url : extractedUrlSet) {
			String href = url.toLowerCase();
			if (href.startsWith("http://www.nytimes.com/") || href.startsWith("https://www.nytimes.com/")
					|| href.startsWith("http://nytimes.com/") || href.startsWith("https://nytimes.com/")) {
				uniqueURLsWithinNewsSite++;
			}
		}
		long uniqueURLsOutsideNewsSite = extractedUrlSet.size() - uniqueURLsWithinNewsSite;
		reportTxtFile.println("Total URLs extracted: " + totalNumberOfExtractedUrls);
		reportTxtFile.println("# unique URLs extracted: " + extractedUrlSet.size());
		reportTxtFile.println("# unique URLs within News Site: " + uniqueURLsWithinNewsSite);
		reportTxtFile.println("# unique URLs outside News Site: " + uniqueURLsOutsideNewsSite);
		reportTxtFile.println();

		reportTxtFile.println("Status Codes:");
		reportTxtFile.println("=============");
		Map<Integer, Long> statusMap = new HashMap<Integer, Long>();
		for (Object localData : crawlersLocalData) {
			CrawlStat stat = (CrawlStat) localData;
			List<FetchStat> fetchedUrlStatList = stat.getFetchedUrlStatList();
			for (FetchStat fetchStat : fetchedUrlStatList) {
				Long count = statusMap.get(fetchStat.getStatusCode());
				if (count == null) {
					count = 1L;
				} else {
					count++;
				}
				statusMap.put(fetchStat.getStatusCode(), count);
			}
		}
		// sorted map
		TreeMap<Integer, Long> tm = new TreeMap<Integer, Long>(statusMap);
		for (Map.Entry<Integer, Long> entry : tm.entrySet()) {
			reportTxtFile.println(String.format("%s %s: %s", entry.getKey(),
					EnglishReasonPhraseCatalog.INSTANCE.getReason(entry.getKey(), Locale.ENGLISH), entry.getValue()));
		}
		reportTxtFile.println();

		reportTxtFile.println("File Sizes:");
		reportTxtFile.println("===========");
		long lessThanOneKB = 0;
		long oneKBToTenKB = 0;
		long tenKBToHundredKB = 0;
		long hundredKBToOneMB = 0;
		long moreThanOneMB = 0;
		for (Object localData : crawlersLocalData) {
			CrawlStat stat = (CrawlStat) localData;
			List<VisitStat> visitedUrlStatList = stat.getVisitedUrlStatList();
			for (VisitStat vstat : visitedUrlStatList) {
				if (vstat.getSize() < 1024) {
					lessThanOneKB++;
				} else if (vstat.getSize() < 10240) {
					oneKBToTenKB++;
				} else if (vstat.getSize() < 102400) {
					tenKBToHundredKB++;
				} else if (vstat.getSize() < (1024 * 1024)) {
					hundredKBToOneMB++;
				} else {
					moreThanOneMB++;
				}
			}
		}
		reportTxtFile.println("< 1KB: " + lessThanOneKB);
		reportTxtFile.println("1KB ~ <10KB: " + oneKBToTenKB);
		reportTxtFile.println("10KB ~ <100KB: " + tenKBToHundredKB);
		reportTxtFile.println("100KB ~ <1MB: " + hundredKBToOneMB);
		reportTxtFile.println(">= 1MB: " + moreThanOneMB);
		reportTxtFile.println();

		reportTxtFile.println("Content Types:");
		reportTxtFile.println("==============");
		Map<String, Long> contentTypeMap = new HashMap<String, Long>();
		for (Object localData : crawlersLocalData) {
			CrawlStat stat = (CrawlStat) localData;
			List<VisitStat> visitedUrlStatList = stat.getVisitedUrlStatList();
			for (VisitStat vstat : visitedUrlStatList) {
				Long count = contentTypeMap.get(vstat.getContentType());
				if (count == null) {
					count = 1L;
				} else {
					count++;
				}
				contentTypeMap.put(vstat.getContentType(), count);
			}
		}
		for (Entry<String, Long> entry : contentTypeMap.entrySet()) {
			reportTxtFile.println(entry.getKey() + ": " + entry.getValue());
		}

		reportTxtFile.close();
	}

	public static void writeFetchFile(List<Object> crawlersLocalData, String fetchCsvFileName) throws IOException {
		PrintWriter fetchCsvFile = new PrintWriter(new FileWriter(fetchCsvFileName));

		// write header
		// fetchCsvFile.println("URL,HTTP status code received");

		for (Object localData : crawlersLocalData) {
			CrawlStat stat = (CrawlStat) localData;
			List<FetchStat> fetchedUrlStatList = stat.getFetchedUrlStatList();
			for (FetchStat fetchStat : fetchedUrlStatList) {
				fetchCsvFile.println(fetchStat.getUrl().replaceAll(",", "_") + "," + fetchStat.getStatusCode());
			}
		}
		fetchCsvFile.close();
	}

	public static void writeVisitFile(List<Object> crawlersLocalData, String visitCsvFileName) throws IOException {
		PrintWriter visitCsvFile = new PrintWriter(new FileWriter(visitCsvFileName));

		// write header
		// visitCsvFile.println("URLs successfully downloaded,Size of the
		// downloaded file,Number of outlinks found,Content-type");

		for (Object localData : crawlersLocalData) {
			CrawlStat stat = (CrawlStat) localData;

			List<VisitStat> visitedUrlStatList = stat.getVisitedUrlStatList();
			for (VisitStat visitStat : visitedUrlStatList) {
				visitCsvFile.println(visitStat.getUrl().replaceAll(",", "_") + "," + visitStat.getSize() + ","
						+ visitStat.getNumberOfOutlinks() + "," + visitStat.getContentType());
			}
		}
		visitCsvFile.close();
	}
}
