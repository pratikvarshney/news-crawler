package edu.usc.csci572.pvarshne.hw2;

public class VisitStat {
	private String url;
	private long size;
	private long numberOfOutlinks;
	private String contentType;

	public VisitStat(String url, long size, long numberOfOutlinks, String contentType) {
		this.url = url;
		this.size = size;
		this.numberOfOutlinks = numberOfOutlinks;
		this.contentType = contentType;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getNumberOfOutlinks() {
		return numberOfOutlinks;
	}

	public void setNumberOfOutlinks(long numberOfOutlinks) {
		this.numberOfOutlinks = numberOfOutlinks;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
