package edu.usc.csci572.pvarshne.hw2;

public class FetchStat {
	private String url;
	private int statusCode;

	public FetchStat(String url, int statusCode) {
		this.url = url;
		this.statusCode = statusCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
}
